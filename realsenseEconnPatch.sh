#!/bin/bash

echo "PLEASE MAKE SURE TO GENERATE A SSH KEY AND ADD TO YOUR BITBUCKET ACCOUNT"
echo ""
echo "ssh-keygen -t rsa -b 4096 -C \"your_email@example.com\""
echo ""


# Econ vars
ethernet_ip=$(ip addr show eth0 | grep "inet\b" | awk '{print $2}' | cut -d/ -f1)
econn_filename=e-CAM130_CUXVR_JETSON_AGX_XAVIER_L4T32.1_26-Apr-2019_R02

#Realsense vars
INSTALL_DIR=$PWD
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

# Is this the correct kernel version?
source scripts/jetson_variables.sh
#Print Jetson version
echo "$JETSON_DESCRIPTION"
#Print Jetpack version
echo "Jetpack $JETSON_JETPACK [L4T $JETSON_L4T]"
echo "Jetson $JETSON_BOARD Development Kit"

# Error out if something goes wrong
set -e

# Check to make sure we're installing the correct kernel sources
# Determine the correct kernel version
# The KERNEL_BUILD_VERSION is the release tag for the JetsonHacks buildKernel repository
KERNEL_BUILD_VERSION=master
# Quotes around Jetson Board because the name may have a space, ie "AGX Xavier"
if [ "$JETSON_BOARD" == "AGX Xavier" ] ; then 
  L4TTarget="32.1.0"
  # Test for 32.1.0 first
  if [ $JETSON_L4T = "32.1.0" ] ; then
     KERNEL_BUILD_VERSION=vL4T32.1.0
  else
   echo ""
   tput setaf 1
   echo "==== L4T Kernel Version Mismatch! ============="
   tput sgr0
   echo ""
   echo "This repository is for modifying the kernel for a L4T "$L4TTarget "system." 
   echo "You are attempting to modify a L4T "$JETSON_L4T "system."
   echo "The L4T releases must match!"
   echo ""
   echo "There may be versions in the tag/release sections that meet your needs"
   echo ""
   exit 1
  fi
fi

# If we didn't find a correctly configured TX2 or TX1 exit, we don't know what to do
if [ $KERNEL_BUILD_VERSION = "master" ] ; then
   tput setaf 1
   echo "==== L4T Kernel Version Mismatch! ============="
   tput sgr0
    echo "Currently this script works for the Jetson AGX Xavier."
   echo "This processor appears to be a Jetson $JETSON_BOARD, which does not have a corresponding script"
   echo ""
   echo "Exiting"
   exit 1
fi

# Switch back to the script directory
cd $INSTALL_DIR

# Get the kernel sources; does not open up editor on .config file
echo "${green}Getting Kernel sources${reset}"
sudo ./scripts/getKernelSourcesNoGUI.sh

echo "${green}Patching and configuring kernel${reset}"
sudo ./scripts/configureKernel.sh
sudo ./scripts/patchKernel.sh

# Download the econn patches
echo "DOWNLOAD THE ECONN FILES ON HOST DEVICE THEN SCP TO JETSON BY RUNNING THE FOLLOWING COMMAND (ON HOST): "
echo ""
echo "scp ~/Downloads/$econn_filename.tar.gz thirdinsight@$ethernet_ip:/home/thirdinsight/buildLibrealsense2Xavier/$econn_filename.tar.gz"
echo ""

# Wait for the file to be transferred
read  -n 1 -p "CLICK ANY KEY ONCE FINISHED" mainmenuinput
echo ""

cd $INSTALL_DIR
# Untar the econ files
echo "Untaring econ files..."
tar -xaf $econn_filename
echo "...complete."

cd 
# Apply the realsense patches
cd /usr/src/
patch -p1 -i /home/thirdinsight/buildLibrealsense2Xavier/$econn_filename/Kernel/Source/e-CAM130_CUXVR_JETSON_AGX_XAVIER_L4T32.1_kernel.patch
patch -p2 -i /home/thirdinsight/buildLibrealsense2Xavier/$econn_filename/Kernel/Source/e-CAM130_CUXVR_JETSON_AGX_XAVIER_L4T32.1_dtb.patch

# Make the new Image and build the modules
echo "${green}Building Kernel and Modules then installing Modules${reset}"
sudo ./scripts/makeKernel.sh

# The user still needs to flash the new kernel ...
echo "${green}Please flash the new kernel Image file on to the Jetson.${reset}"
echo "${green}The new kernel Image is in the directory named 'image'.${reset}"

mkdir -p image
cp /usr/src/kernel/kernel-4.9/arch/arm64/boot/Image ./image/Image




